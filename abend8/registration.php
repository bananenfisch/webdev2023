<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    /*
        1. Prüfe, ob die Datei über das Form-Submit aufgerufen wurde:
            Wenn ja:
                2. Prüfe folgendes:
                - email gesetzt ist und nicht leer (Bspw. != '' oder mit strlen)
                - pwd gesetzt ist und nicht leer (Bspw. != '' oder mit strlen)
                - pwd_confirm gesetzt ist und nicht leer (Bspw. != '' oder mit strlen)
                - ob pwd und pwd_confirm gleich ist
                    Wenn ja: gib "OK"
            Wenn ein Fehler: gib "Fehler" aus (wenn Zeit: evtl. auch welcher Fehler)
    */
    ?>
    <form action="" method="POST">
        E-Mail: <input type="text" name="mail"><br>
        Passwort: <input type="password" name="pwd"><br>
        Passwort wiederholen: <input type="password" name="pwd_confirm"><br>
        <input type="submit" value="Absenden">
    </form>
</body>
</html>
