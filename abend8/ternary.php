<?php
// https://www.w3schools.com/php/php_operators.asp

$x = 1;

// Langfassung:
if ($x != 0) {
    echo $x;
} else {
    echo 'Division nicht möglich: Wert ist 0';
}

echo '<hr>';

// Abkürzung:
echo ($x != 0) ? $x : 'Division nicht möglich: Wert ist 0';
