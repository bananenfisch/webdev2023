<?php
// https://www.w3schools.com/php/php_operators.asp
$x = 1;

/*
// Langfassung - solche Konstrukte sind oft in Verwendung:
if (isset($x)) {
    echo $x;
} else {
    echo 'bla';
}
*/

// Daher gibts es in PHP hier einen eigenen "Abkürzungs" Operator (Null coalescing assignment operator):
echo $x ?? 'bla';
