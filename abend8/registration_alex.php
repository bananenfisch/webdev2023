<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="" method="post">
    <!-- type="email" ist besser - text nur für fehlertesten -->
    <input type="text" name="email" placeholder="email" value="<?= $_POST['email'] ?? '' ?>">
    <input type="password" name="password" placeholder="Passwort">
    <input type="password" name="password1" placeholder="Passwort wiederholen">
    <input type="file" name="img" placeholder="Bild">
    <input type="submit">
</form>

<?php
    // Warum ist serverseitige validierung überhaupt wichtig?!
    // - Weil clientseitige Validierung (HTML, JavaScript) manipulierbar ist!

    // Abkürzungsvariante:
    // if (count($_POST) > 0) {
    if (isset($_POST['email']) && isset($_POST['password']) && isset($_POST['password1'])) {

        // htmlspecialchars: kann man natürlich schon beim Speichern anwenden.
        // Vorsicht: in manchen Fällen kann das zu Problemen führen - bspw. wenn die gespeicherten Daten in einem anderen Format (PDF, XLS) ausgegeben werden sollen
        // dann hätte man bspw. in der XLS Spalte z.B.: "user&lt;test"
        // Daher ist es oft besser so: Daten speichern "as it is" - je nach Ausgabeformat dann formatieren
        // D.h.: Variante bedeutet: nicht beim speichern htmlspecialchars, sondern bei der Ausgabe von Daten!
        $inputMail = htmlspecialchars($_POST['email']);
        $inputPassword = htmlspecialchars($_POST['password']);
        $inputPassword1 = htmlspecialchars($_POST['password1']);

        $error = '';

        if (strlen($inputMail) < 5 || strlen($inputPassword) < 5 || strlen($inputPassword1) < 5) {
            // $error .= 'Text';
            // Ist eine Kurzform, d.h. dasselbe wie dieser Befehl:
            // $error = $error . 'Text';
            $error .= 'Bitte mind. 5 Zeichen eingeben<br>';
        }
        if ($inputPassword != $inputPassword1) {
            $error .= 'Passwort nicht gleich!<br>';
        }
        if (!filter_var($inputMail, FILTER_VALIDATE_EMAIL)) {
            $error .= 'Bitte richtige Email angeben!<br>';
        }

        if ($error === '') {
            // speichern
            echo "Vielen Dank!";
        } else {
            echo $error;
        }

    }

?>
</body>
</html>
