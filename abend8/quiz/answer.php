<?php
require 'inc/config.php';
require 'inc/data.php';
include 'templates/header.php';

/*
array(1) {
  ["answer"]=>
  array(0) {
    [0]=>
    string(1) "2"
    [1]=>
    string(1) "3"
  }
}
*/

$question = $questions[0];
?>
    <h1>Result</h1>
    <p><?= htmlspecialchars($question['text']) ?></p>
    <table class="table">
<?php


$counter = 0;
// Durchlauf über alle Antworten
foreach ($question['answers'] as $answer) {
    echo '<tr>';
    echo '<td>' . htmlspecialchars($answer['text']) . '</td>';

    // 1. Prüfe, ob User die jeweilige Antwort angeklickt hat:
    // - indem geprüft wird, ob der Index im POST Feld übertragen wurde
    // $_POST['answer'] -> ist hier $counter drin?
    // 2a. Wenn der Index übertragen wurde: prüfe, ob correct true ist -> RICHTIG!
    // 2b. Wenn der Index übertragen wurde: prüfe, ob correct false ist -> FALSCH!
    // 2c. Wenn der Index nicht übertragen wurde: prüfe, ob correct true ist -> FALSCH!
    // 2d. Wenn der Index nicht übertragen wurde: prüfe, ob correct false ist -> RICHTIG!

    // 2a + 2b
    // besser wie alex meinte: if isset && ...
    if (isset($_POST['answer']) && in_array($counter, $_POST['answer'])) {
        // 2a
        if ($answer['correct']) {
            echo '<td style="background-color: green">X</td>';
        }
        // 2b
        else {
            echo '<td style="background-color: red">X</td>';
        }
    }
    // 2c + 2d
    else {
       // 2c
       if ($answer['correct']) {
        echo '<td style="background-color: red"></td>';
        }
        // 2d
        else {
            echo '<td style="background-color: green"></td>';
        }
    }

    // echo '<td> USEREINGABE - wenn richtig: grün, wenn falsch rot <td>';
    echo '</tr>';
    $counter++;
}
?>
    </table>
<?php
include 'templates/footer.php';
?>
