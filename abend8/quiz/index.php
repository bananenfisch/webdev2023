<?php
require 'inc/config.php';
require 'inc/data.php';
include 'templates/header.php';

// Möglichkeit: speichere die Frage in eine Variable (spart uns unten im Code die 1. Ebene direkt ansprechen zu müssen)
$question = $questions[0];
?>
        <h1>Willkommen auf unserer <?= APPNAME ?> Applikation</h1>

        <p>Frage 1</p>
        <p><?= htmlspecialchars($question['text']) ?></p>

        <form action="answer.php" method="POST">

            <?php
            // 1. foreach über alle answers
            // 2. counter = 0
            // 3. in der schleife: btn ausgabe mit id counter; sowie antworttext
            // 4. counter hochzählen
            $counter = 0;
            foreach ($question['answers'] as $answer) {
            ?>
                <div class="mb-3">
                    <input type="checkbox" class="btn-check" name="answer[]" value="<?= $counter ?>" id="button<?= $counter ?>">
                    <label class="btn btn-outline-success" for="button<?= $counter ?>"><?= htmlspecialchars($answer['text']) ?></label>
                </div>
            <?php
                $counter++;
            }
            ?>

            <div class="mb-3">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>

        </form>
<?php
include 'templates/footer.php';
?>
