<?php
// Exkurs (nicht Basis-Relevant):
// Wenn Daten außerhalb von document_root liegen, kann man über HTTP (Browser) nicht darauf zugreifen,
// - ist sinnvoll: Sicherheit bei Uploads bspw.
// Um diese Daten aber doch anzeigen zu lassen, kann über PHP die Datei direkt eingelesen werden, und ausgegeben werde:
$file = '../../upload/katze.jpg';
$type = 'image/jpeg';
header('Content-Type:'.$type);
header('Content-Length: ' . filesize($file));
readfile($file);
