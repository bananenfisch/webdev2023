<!doctype html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="" method="post" enctype="multipart/form-data">
    <!-- type="email" ist besser - text nur für fehlertesten -->
    <input type="text" name="email" placeholder="email" value="<?= $_POST['email'] ?? '' ?>">
    <input type="password" name="password" placeholder="Passwort">
    <input type="password" name="password1" placeholder="Passwort wiederholen">
    <input type="file" name="img" placeholder="Bild">
    <input type="submit">
</form>

<?php
    if (count($_POST) > 0) {
        $inputMail = $_POST['email'];
        $inputPassword = $_POST['password'];
        $inputPassword1 = $_POST['password1'];

        $error = '';

        if (mb_strlen($inputMail) < 5 || mb_strlen($inputPassword) < 5 || mb_strlen($inputPassword1) < 5) {
            $error .= 'Bitte mind. 5 Zeichen eingeben<br>';
        }
        if ($inputPassword != $inputPassword1) {
            $error .= 'Passwort nicht gleich!<br>';
        }
        if (!filter_var($inputMail, FILTER_VALIDATE_EMAIL)) {
            $error .= 'Bitte richtige Email angeben!<br>';
        }

        /*
            Bei Uploads, 3 Problemfelder:
            1. Schadcode könnte hochgeladen und ausgeführt werden!
            2. Directory Listing könnte "on" sein! Bzw. Hochgeladene Daten Dritter könnten eingesehen werden.
            3. Daten könnten überschrieben werden (bei gleichem Dateinamen bspw.)

            Annahme:
            a.) Wir wollen jpg, jpeg, png, gif, svg, pdf, docx als Formate erlauben!
            b.) Wir wollen für den Upload Ordner kein Directory Listing erlauben!
                (siehe .htaccess im Upload Ordner - done!)
            c.) Wir wollen Daten nicht überschreiben!
            d.) Zusätzlich: keine Script Execution im Upload Ordner erlauben!
                (siehe .htaccess im Upload Ordner - done!)
        */

        // a.) Hole die Dateiendung:
        $filename = basename($_FILES['img']['name']);
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $extension = strtolower($extension);
        $allowed_extensions = [ 'jpg', 'jpeg', 'png', 'gif', 'svg', 'pdf', 'docx' ];
        if (in_array($extension, $allowed_extensions)) {
            if ($error === '') {
                // c.)
                // wir speichern nicht unter dem ursprünglichen Namen,
                // sondern bspw. mit einer eindeutigen ID . extension
                // Das hat zwei Vorteile:
                // 1. Daten werden nicht überschrieben!
                // 2. Falls die Ursprungsdatei Sonderzeichen eines anderen Zeichensatzes enthält, könnte das Probleme machen.
                $target = 'upload/' . uniqid() . ".$extension";
                // 'upload/katze2323.jpg'
                move_uploaded_file($_FILES['img']['tmp_name'], $target);
            }
        } else {
            $error .= "Datentyp nicht erlaubt!";
        }

        if ($error === '') {
            // TODO:
            // 1. Setze Daten in ein Array
            // 2. Wichtig: Passwort nicht in Plaintext!
            // 3. CSV speichern:
            // 3a. Datei öffnen!
            // 3b. Daten schreiben!

            // 1. und 2.:
            $row = [
                $inputMail,
                password_hash($inputPassword, PASSWORD_DEFAULT),
                $target
            ];
            $fp = fopen('data.csv', 'a');
            fputcsv($fp, $row);
            // fputcsv -> Array -> CSV File
            // fgetcsv -> read CSV File -> Array
            echo "Vielen Dank!";
        } else {
            echo $error;
        }
    }

?>
</body>
</html>
