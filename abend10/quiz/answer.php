<?php
session_start();
require 'inc/config.php';
protect_site();
require 'inc/data.php';
include 'templates/header.php';

$diff = round(microtime(true) - $_SESSION['time'], 2);
$question = $questions[$_SESSION['question']];
?>
    <h1><?= $_SESSION['username'] ?> - Your Result</h1>
    <p><?= _ht($question['text']) ?></p>
    <table class="table">
<?php
$counter = 0;
$bingo = true;
foreach ($question['answers'] as $answer) {
    echo '<tr>';
    echo '<td>' . _ht($answer['text']) . '</td>';
    if (isset($_POST['answer']) && in_array($counter, $_POST['answer'])) {
        if ($answer['correct']) {
            // TODO: besser: class...
            echo '<td style="background-color: green">✓</td>';
        }
        else {
            echo '<td style="background-color: red">✓</td>';
            $bingo = false;
        }
    }
    else {
       if ($answer['correct']) {
            echo '<td style="background-color: red"></td>';
            $bingo = false;
        }
        else {
            echo '<td style="background-color: green"></td>';
        }
    }
    echo '</tr>';
    $counter++;
}
?>
    </table>
    <?php
    if ($bingo) {
    ?>
        <div class="alert alert-success" role="alert">
            BINGO!
        </div>
    <?php
    } else {
    ?>
        <div class="alert alert-danger" role="alert">
            Nicht ganz!
        </div>
    <?php
    }
    ?>
    <div class="alert alert-light" role="alert">
        Du hast <?= $diff ?> Sekunden für diese Antwort gebraucht!
        <?php
        if ($diff < 5) {
            echo 'Wahnsinn!';
        }
        elseif ($diff < 10) {
            echo 'Sehr gut!';
        }
        elseif ($diff < 20) {
            echo 'Ganz okay!';
        }
        else {
            echo 'Ernsthaft? Eingeschlafen?';
        }
        ?>
    </div>
    <a href="question.php" class="btn btn-primary">Weiter!</a>
<?php
include 'templates/footer.php';
