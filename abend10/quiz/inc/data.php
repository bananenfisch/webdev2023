<?php
$questions = [];

$questions[] = [
    'text'    => 'Was ist ein gültiger Variablenname in PHP?',
    'answers' => [
        [ 'text' => '$txt_user_1', 'correct' => true ],
        [ 'text' => '$TxtUser1', 'correct' => true ],
        [ 'text' => 'txt_user_1', 'correct' => false ],
        [ 'text' => '$txt.user.1', 'correct' => false ],
    ],
];

$questions[] = [
    'text'    => 'Wie kann ein Kommentar im PHP Code eingefügt werden?',
    'answers' => [
        [ 'text' => '/* Kommentar */', 'correct' => true ],
        [ 'text' => '// Kommentar', 'correct' => true ],
        [ 'text' => '# Kommentar', 'correct' => true ],
        [ 'text' => '<!-- Kommentar -->', 'correct' => false ],
    ],
];

$questions[] = [
    'text'    => 'Welche Datentypen kennt PHP?',
    'answers' => [
        [ 'text' => 'String, Boolean, Integer, Float, Array, Object, NULL', 'correct' => true ],
        [ 'text' => 'String, Boolean, Number, Object, Undefined, Empty', 'correct' => false ],
        [ 'text' => 'TinyText, Text, Boolean, Integer, BigInteger, Double, Object, Array', 'correct' => false ],
        [ 'text' => 'Function, Class, Primitives, Empty', 'correct' => false ],
    ],
];

$questions[] = [
    'text'    => 'Welcher Operator wird verwendet um ungleiche Werte zu erkennen?',
    'answers' => [
        [ 'text' => '!=', 'correct' => true ],
        [ 'text' => '!==', 'correct' => true ],
        [ 'text' => '<=>', 'correct' => false ],
        [ 'text' => '==', 'correct' => false ],
    ],
];

$questions[] = [
    'text'    => 'Wofür steht der Operator &&?',
    'answers' => [
        [ 'text' => 'AND', 'correct' => true ],
        [ 'text' => 'Potenzieren', 'correct' => false ],
        [ 'text' => 'Modulo', 'correct' => false ],
        [ 'text' => 'XOR', 'correct' => false ],
    ],
];
