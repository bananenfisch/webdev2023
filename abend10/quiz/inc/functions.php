<?php
// Hier folgen die Funktionen für unsere Web-App!
function _ht(string $output)
{
    return htmlspecialchars($output);
}
function protect_site()
{
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
        header('location: index.php');
        exit; // WICHTIG!!!
    }
}
