<?php
session_start();
require_once 'inc/config.php';
protect_site();
require 'inc/data.php';
include 'templates/header.php';

// random question! (und save in session, sowie timestamp!)
$count = count($questions);
$rand = rand(0, $count-1);

$_SESSION['question'] = $rand;
$_SESSION['time'] = microtime(true);

$question = $questions[$rand];
?>
        <h1>Willkommen auf unserer <?= APPNAME ?> Applikation</h1>

        <p>Frage 1</p>
        <p><?= _ht($question['text']) ?></p>

        <form action="answer.php" method="POST">

            <?php
            $counter = 0;
            foreach ($question['answers'] as $answer) {
            ?>
                <div class="mb-3">
                    <input type="checkbox" class="btn-check" name="answer[]" value="<?= $counter ?>" id="button<?= $counter ?>">
                    <label class="btn btn-outline-success" for="button<?= $counter ?>"><?= _ht($answer['text']) ?></label>
                </div>
            <?php
                $counter++;
            }
            ?>

            <div class="mb-3">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>

        </form>
<?php
include 'templates/footer.php';
