<?php
require_once 'inc/config.php';

$error = '';
if (count($_POST) > 0) {
    if (mb_strlen($_POST['username']) > 2) {
        session_start();
        $_SESSION['username'] = $_POST['username'];
        $_SESSION['loggedin'] = true;
        header('location: question.php');
        exit;
    }
    else {
        $error = 'Bitte mind. 3 Zeichen.';
    }
}

include 'templates/header.php';
echo $error;
?>
<h1>Willkommen auf unserer <?= APPNAME ?> Applikation</h1>
<form action="" method="POST">
    <div class="mb-3">
        <label for="username" class="form-label">Username</label>
        <input type="text" name="username" class="form-control" id="username">
    </div>
    <button type="submit" class="btn btn-primary">START!</button>
</form>
<?php
include 'templates/footer.php';
