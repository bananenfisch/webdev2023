<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=#, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
    // ÜBUNG:
    // enwicklung der function: get_inflation - immer bezogen auf das letzte jahr im array!
    // zusätzliche übungsmöglichkeit get_inflation mit zusätzlichem parameter (optional): welches jahr! (hinweis: wenn key == jahr: break!)

    function get_inflation(int|float $warenkorb, array $inflation, ?int $j = null)
    {
//        $warenkorb += ($warenkorb / 100) * $inflation['2024'];
//        $warenkorb += ($warenkorb / 100) * $inflation['2025'];
//        $warenkorb += ($warenkorb/ 100) * $inflation['2026'];
//        return $warenkorb;

        foreach ($inflation as $key => $value){

            $warenkorb += ($warenkorb /100) * $value;

            if ($key == $j){
                break;
            }
        }
        return $warenkorb;
    }

    $inflation_best_case = [
        '2024' => 3.6,
        '2025' => 2.8,
        '2026' => 2.2,
    ];
    $inflation_realistic_case = [
        '2024' => 4.2,
        '2025' => 3.6,
        '2026' => 2.9,
    ];
    $inflation_worst_case = [
        '2024' => 6.2,
        '2025' => 5.8,
        '2026' => 4.2,
    ];
    $warenkorb = 100;


    // Beispiel 1: Wert des Warenkorbs bei $inflation_best_case im Jahr 2026:
    echo "Im Jahr 2026 entsprechen EUR $warenkorb die Summe von EUR " . get_inflation($warenkorb, $inflation_best_case, 2024);
    echo '<hr>';
    // Beispiel 2: Wert des Warenkorbs bei $inflation_realistic_case im Jahr 2026:
    echo "Im Jahr 2026 entsprechen EUR $warenkorb die Summe von EUR " . get_inflation($warenkorb, $inflation_realistic_case);
    echo '<hr>';
    // Beispiel 3: Wert des Warenkorbs bei $inflation_worst_case im Jahr 2026:
    echo "Im Jahr 2026 entsprechen EUR $warenkorb die Summe von EUR " . get_inflation($warenkorb, $inflation_worst_case);
?>

</body>
</html>
