<?php

$array = [ 2, 5, 6, 8 ];
echo myCount($array);

echo '<hr>';

echo addiere(1.5, 4);

// echo count($array);

function myCount(array $a) : int
{
    $i = 0;
    foreach ($a as $key) {
        $i++;
    }
    return $i;
}

function addiere(int|float $x, int|float $y) : int|float
{
    return $x + $y;
}
