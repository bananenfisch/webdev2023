<?php
/**
 * Operatoren - neue!
 */

// Vollständige Liste, siehe: https://www.w3schools.com/php/php_operators.asp

// Modulus:
$x = 5;
if ($x % 2 === 0) {
    echo 'Zahl ist gerade';
} else {
    echo 'Zahl ist ungerade';
}

echo '<hr>';

// Spaceship:
var_dump ( 4 <=> 4 );

echo '<hr>';

// OR:
var_dump ( true || true ); // -> true

// XOR:
var_dump ( true xor true); // -> false
