<?php
$questions = [];

$questions[] = [
    'text'    => 'Was ist ein gültiger Variablenname in PHP?',
    'answers' => [
        [ 'text' => '$txt_user_1', 'correct' => true ],
        [ 'text' => '$TxtUser1', 'correct' => true ],
        [ 'text' => 'txt_user_1', 'correct' => false ],
        [ 'text' => '$txt.user.1', 'correct' => false ],
    ],
];

$questions[] = [
    'text'    => 'Wie kann ein Kommentar im PHP Code eingefügt werden?',
    'answers' => [
        [ 'text' => '/* Kommentar */', 'correct' => true ],
        [ 'text' => '// Kommentar', 'correct' => true ],
        [ 'text' => '# Kommentar', 'correct' => true ],
        [ 'text' => '<!-- Kommentar -->', 'correct' => false ],
    ],
];
